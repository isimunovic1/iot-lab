package main

import "fmt"

func main(){
    var broj1, broj2, broj3 int
    fmt.Printf("Unesite prvi prirodan broj: ")
    fmt.Scan(&broj1)

    fmt.Printf("Unesite drugi prirodan broj: ")
    fmt.Scan(&broj2)

    fmt.Printf("Unesite treci prirodan broj: ")
    fmt.Scan(&broj3)

    if broj1 < broj2 && broj1 < broj3{
		fmt.Println(broj1)
		if broj2 < broj3{
			fmt.Println(broj2)
			fmt.Println(broj3)
		} else{
			fmt.Println(broj3)
			fmt.Println(broj2)
		}
	} else if broj2 < broj1 && broj2 < broj3{
		fmt.Println(broj2)
		if broj1 < broj3{
			fmt.Println(broj1)
			fmt.Println(broj3)
		} else{
			fmt.Println(broj3)
			fmt.Println(broj1)
		}
	} else{
		fmt.Println(broj3)
		if broj1 < broj2{
			fmt.Println(broj1)
			fmt.Println(broj2)
		} else{
			fmt.Println(broj2)
			fmt.Println(broj1)
		}
	}
}