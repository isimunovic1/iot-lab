package main
import "fmt"

func prikazElemenata(slice []int){
	fmt.Println(slice)
}

func dodajElement(slice []int, broj int) []int{
	return append(slice, broj)
}

func main(){

	intSlice := []int{1,2,3,4,5}
	intSlice = dodajElement(intSlice, 6)
	/*
	for i, v := range intSlice {
		fmt.Println("Indeks: ", i, ", vrijednost: ", v)
	}*/
	prikazElemenata(intSlice)
}