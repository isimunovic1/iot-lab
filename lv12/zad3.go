package main
import "fmt"
import "time"
import "strconv"
type TemperaturniSenzor struct {
	ID int
	vrijednost float64
	vrijemeDohvacanja time.Time
}

func prikazVrijednosti(temperatura TemperaturniSenzor) string{
	return strconv.FormatFloat(temperatura.vrijednost, 'f', 4, 64) + "°C"
}
func main(){
	temperatura := TemperaturniSenzor{1, 23.333333, time.Now()}
	fmt.Println(prikazVrijednosti(temperatura))
}
